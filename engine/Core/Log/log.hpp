/**
 * @file "Core/Log/log.hpp"
 * This file contain all macro to use log system
 * To activate the log, the definition EZ_LOG must be declared
 *
 * @author Alexandre Chetafi
 */

#ifndef ENGINE_LOG_HPP
#define ENGINE_LOG_HPP

#	define EZ_INFO 0
#	define EZ_WARN 1
#	define EZ_ERROR 2
#	define EZ_CRITICAL 3
#	define EZ_TRACE 4

#ifdef EZ_LOG

#	include "log_internal.hpp"

#	define LOG(level, string)						level##_LOG(string)
#	define LOG_DETAIL(level, string, file, line) 	level##_LOG_DETAIL(string, file, line)

#	if EZ_LOG >= 0
#	define EZ_INFO_LOG(string)						ez::log::write_terminal(string)
#	define EZ_INFO_LOG_DETAIL(string, file, line)	ez::log::write_terminal(string, ez::log::COLOR::WHITE, ez::log::SGR::NORMAL, line, file))
#	else
#	define EZ_INFO_LOG(...)
#	define EZ_INFO_LOG_DETAIL(...)
#	endif
#	if EZ_LOG >= 1
#	define EZ_WARN_LOG(string)						ez::log::write_terminal(string, ez::log::COLOR::YELLOW)
#	define EZ_WARN_LOG_DETAIL(string, file, line)	ez::log::write_terminal(string, ez::log::COLOR::YELLOW, ez::log::SGR::NORMAL, line, file)
#	else
#	define EZ_WARN_LOG(...)
#	define EZ_WARN_LOG_DETAIL(...)
#	endif
#	if EZ_LOG >= 2
#	define EZ_ERROR_LOG(string)						ez::log::write_terminal(string, ez::log::COLOR::RED)
#	define EZ_ERROR_LOG_DETAIL(string, file, line)	ez::log::write_terminal(string, ez::log::COLOR::RED, ez::log::SGR::NORMAL, line, file)
#	else
#	define EZ_ERROR_LOG(...)
#	define EZ_ERROR_LOG_DETAIL(...)
#	endif
#	if EZ_LOG >= 3
#	define EZ_CRITICAL_LOG(string)						ez::log::write_terminal(string, ez::log::COLOR::GREEN)
#	define EZ_CRITICAL_LOG_DETAIL(string, file, line)	ez::log::write_terminal(string, ez::log::COLOR::GREEN, ez::log::SGR::NORMAL, line, file)
#	else
#	define EZ_CRITICAL_LOG(...)
#	define EZ_CRITICAL_LOG_DETAIL(...)
#	endif
#	if EZ_LOG >= 4
#	define EZ_TRACE_LOG(string)						ez::log::write_terminal(string, ez::log::COLOR::CYAN, ez::log::SGR::UNDERLINE)
#	define EZ_TRACE_LOG_DETAIL(string, file, line)	ez::log::write_terminal(string, ez::log::COLOR::CYAN, ez::log::SGR::UNDERLINE, line, file)
#	else
#	define EZ_TRACE_LOG(...)
#	define EZ_TRACE_LOG_DETAIL(...)
#	endif

#else
#	define LOG(...)
#	define LOG_DETAIL(...)
#	define EZ_INFO_LOG(...)
#	define EZ_WARN_LOG(...)
#	define EZ_ERROR_LOG(...)
#	define EZ_CRITICAL_LOG(...)
#	define EZ_TRACE_LOG(...)
#	define EZ_INFO_LOG_DETAIL(...)
#	define EZ_WARN_LOG_DETAIL(...)
#	define EZ_ERROR_LOG_DETAIL(...)
#	define EZ_CRITICAL_LOG_DETAIL(...)
#	define EZ_TRACE_LOG_DETAIL(...)
#endif

#endif //ENGINE_LOG_HPP
