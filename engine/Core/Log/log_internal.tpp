/**
 * @file "Core/Log/log_internal.tpp"
 *
 * @author Alexandre Chetafi
 */

#pragma once

#ifndef ENGINE_LOG_INTERNAL_TPP
#define ENGINE_LOG_INTERNAL_TPP

#include <iostream>
#include <ctime>

namespace ez
{
#	ifdef _MSC_VER
	namespace {
		HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	}
#	endif
	template <typename CharT, typename Traits, typename Allocator>
	inline void log::write_terminal(const std::basic_string<CharT, Traits, Allocator> &st, COLOR c, SGR s, int line, const char *file)
	{

#ifdef _MSC_VER
		SetConsoleTextAttribute(hConsole, (uint32_t) (c));
		std::cout
#else
		std::cout << (uint32_t) (s) << ";" << (uint32_t) (c) << "m"
#endif
		<< "[" << log::getTime() << "] " << st;

		if (line >= 0 && file != nullptr) {
			std::cout << "line :" << line << " file : " << file;
		}
#ifdef _MSC_VER
		std::cout << std::endl;
		SetConsoleTextAttribute(hConsole, 7);
#else
		std::cout << "\033[0m" << std::endl;
#endif
	}

	inline void log::write_terminal(const char *st, COLOR c, SGR s, int line, const char *file)
	{
#ifdef _MSC_VER
		SetConsoleTextAttribute(hConsole, (uint32_t) (c));
		std::cout
#else
		std::cout << (uint32_t) (s) << ";" << (uint32_t) (c) << "m"
#endif
		<< "[" << log::getTime() << "] " << st;
		if (line >= 0 && file != nullptr) {
			std::cout << "line :" << line << " file : " << file;
		}
#ifdef _MSC_VER
		std::cout << std::endl;
		SetConsoleTextAttribute(hConsole, 7);
#else
		std::cout << "\033[0m" << std::endl;
#endif
	}
	std::string log::getTime()
	{
		std::time_t result = std::time(nullptr);
		std::string res(std::ctime(&result));
		return res.empty() ? "" : res.substr(0,res.length() - 1);
	}
}

#endif //ENGINE_LOG_INTERNAL_TPP