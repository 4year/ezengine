/**
 * @file "Core/Log/log_internal.hpp"
 *
 * @author Alexandre Chetafi
 */

#ifndef ENGINE_LOG_INTERNAL_HPP
#define ENGINE_LOG_INTERNAL_HPP

#include <string>
#include "../../config.hpp"

namespace ez
{
	class log
	{
	public:
#ifdef _MSC_VER
		enum class COLOR : uint32_t {
			BLUE			= 1,
			GREEN			= 2,
			CYAN			= 3,
			RED				= 4,
			MAGENTA			= 5,
			YELLOW			= 6,
			WHITE			= 7,
			GREY			= 8,
			BLACK_BRIGHT 	= 8,
			RED_BRIGHT		= 9,
			GREEN_BRIGHT	= 10,
			YELLOW_BRIGHT	= 11,
			BLUE_BRIGHT		= 12,
			MAGENTA_BRIGHT	= 13,
			CYAN_BRIGHT		= 14,
			WHITE_BRIGHT	= 15
		};
#else
		enum class COLOR : uint32_t {
			BLACK			= 30,
			RED				= 31,
			GREEN			= 32,
			YELLOW			= 33,
			BLUE			= 34,
			MAGENTA			= 35,
			CYAN			= 36,
			WHITE			= 37,
			GRAY			= 90,
			BLACK_BRIGHT	= 90,
			RED_BRIGHT		= 91,
			GREEN_BRIGHT	= 92,
			YELLOW_BRIGHT	= 93,
			BLUE_BRIGHT		= 94,
			MAGENTA_BRIGHT	= 95,
			CYAN_BRIGHT		= 96,
			WHITE_BRIGHT	= 97
		};
#endif

		enum class SGR : uint32_t {
			RESET 		= 0,
			NORMAL 		= 0,
			BOLD		= 1,
			FAINT		= 2,
			ITALIC		= 3,
			UNDERLINE	= 4
		};

		template <typename CharT, typename Traits, typename Allocator>
		static inline void write_terminal(const std::basic_string<CharT, Traits, Allocator> &st, COLOR c = COLOR::WHITE, SGR s = SGR::NORMAL, int line = -1, const char *file = nullptr);

		static inline void write_terminal(const char *st, COLOR c = COLOR::WHITE, SGR s = SGR::NORMAL, int line = -1, const char *file = nullptr);
	private:
		static inline std::string getTime();
	};
}

#include "log_internal.tpp"

#endif //ENGINE_LOG_INTERNAL_HPP
